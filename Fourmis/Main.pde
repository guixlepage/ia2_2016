int nbVilles = 5;  
int nbFourmis = 100;
int nbIterations = 1000;
float tauxEvaporation =(float)0.05;

int rayon = 350;
int centre = 400;
ArrayList<Point> listPoints = new ArrayList<Point>();



void settings() {
  size(800, 800);
}

void setup() {
  stroke(255);
  background(0);
  frameRate(30);
  
  creerPointsCirculaires(nbVilles,rayon,centre);
  
  int t1 = millis();
  Probleme problem = new Probleme(nbVilles, tauxEvaporation);
  Colonie colonie = new Colonie(nbFourmis,nbIterations,problem);
  colonie.lancerColonie();
  int t2 = millis();
  
  afficherDistances(problem.tabDistances);
  afficherPheromones(problem.tabPheromones);
  afficherChemin(colonie.villesMeilleurChemin, colonie.meilleureLongueur);
  
  System.out.println("Temps : " + (t2 - t1) + " ms");  //47 ms pour 50 villes, 
}

void draw() {}
    
void creerPointsCirculaires(int nbVilles, int rayon, int centre) {
  double pas = 2 * Math.PI / nbVilles;
  for (int i = 0; i<nbVilles; i++) {
    float pasAngle = (float)pas * i;
    int x = (int)(centre + rayon * cos(pasAngle));
    int y = (int)(centre + rayon * sin(pasAngle));
    listPoints.add(new Point(x,y));
    fill(255,255,255);
    point(x,y);
    if (y < 400)
      text(i,x,y-10);
    else
      text(i,x+10,y);
  }
  for (int i = 0; i < listPoints.size(); i++) {
    for (int j = 0; j < listPoints.size(); j++) {
      line(listPoints.get(i).x, listPoints.get(i).y, listPoints.get(j).x, listPoints.get(j).y);
      
    }
  }
}

void afficherDistances(int[][] tabDistances) {
  for (int i = 0; i < tabDistances.length; i++) {
    for (int j = 0; j < tabDistances[i].length; j++) {
      if (i != j) {
        int x = (int)(listPoints.get(i).x + listPoints.get(j).x)/2;
        int y = (int)(listPoints.get(i).y + listPoints.get(j).y)/2;
        fill(255,0,0);
        text(tabDistances[i][j],x-15,y-20);
      }
    }
  }
}

void afficherPheromones(float[][] tabPheromones) {
  for (int i = 0; i < tabPheromones.length; i++) {
    for (int j = 0; j < tabPheromones[i].length; j++) {
      if (i != j) {
        int x = (int)(listPoints.get(i).x + listPoints.get(j).x)/2;
        int y = (int)(listPoints.get(i).y + listPoints.get(j).y)/2;
        fill(0,tabPheromones[i][j]*2+55,0);
        text(nf((int)tabPheromones[i][j],3),x+15,y+20);
      }
    }
  }
}

void afficherChemin(ArrayList<Integer> villesMeilleurChemin, int longueur) {
  String chemin = "Chemin optimal : ";
  for (int i = 0; i < villesMeilleurChemin.size()-1; i++) {
    strokeWeight(4);
    fill(255,255,0);
    line(listPoints.get(villesMeilleurChemin.get(i)).x,listPoints.get(villesMeilleurChemin.get(i)).y,listPoints.get(villesMeilleurChemin.get(i+1)).x,listPoints.get(villesMeilleurChemin.get(i+1)).y);
    strokeWeight(1);
  }
  for(int j = 0; j < villesMeilleurChemin.size(); j++) {
    if (j < villesMeilleurChemin.size()-1)
      chemin += villesMeilleurChemin.get(j) + " => ";
    else
      chemin += villesMeilleurChemin.get(j);
  }
  textSize(20);
  text(chemin,10,30);
  text("Longueur minimale = " + longueur,10,60);
}