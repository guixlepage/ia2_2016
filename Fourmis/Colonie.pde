import java.util.ArrayList;

public class Colonie {
  public int nbFourmis;
  public ArrayList<Fourmis> listFourmis = new ArrayList<Fourmis>();
  public ArrayList<Integer> villesMeilleurChemin = new ArrayList<Integer>();
  public int meilleureLongueur;
  public Probleme probleme;
  public int nbIterations;
  
  public Colonie(int nbFourmis, int nbIterations, Probleme probleme) {
    this.nbFourmis = nbFourmis;
    this.nbIterations = nbIterations;
    this.meilleureLongueur = 999;
    this.probleme = probleme;
    
    for (int i = 0; i<nbFourmis;i++) {
      Fourmis fourmi = new Fourmis(this.probleme);
      this.listFourmis.add(fourmi);
    }
    
  }
  
  public void lancerColonie() {
    
      for (int j=0; j<listFourmis.size();j++) {
        for (int i = 0; i<this.nbIterations;i++) {
          if (listFourmis.get(j).mouvement()) {
            if (this.meilleureLongueur > listFourmis.get(j).longueurVisitee && listFourmis.get(j).villesVisitees.size() == nbVilles) {
              this.meilleureLongueur = listFourmis.get(j).longueurVisitee;
              this.villesMeilleurChemin = listFourmis.get(j).villesVisitees;
              if (i % 10 == 0 && i > 0)
                probleme.evaporation();
            }
          }
      }
    }
  }
  
  public boolean fourmisTermine() {
    boolean termine = true;
    for (int i = 0 ; i < listFourmis.size(); i++) {
      if (!listFourmis.get(i).termine)
        termine = false;
    }
    return termine;
  }
  
  public void afficherSolution() {   //temporaire
    for (int i = 0; i<this.villesMeilleurChemin.size();i++) {
      System.out.println(villesMeilleurChemin.get(i));
    }
    System.out.println("Longueur minimale : " + this.meilleureLongueur);
  }
}