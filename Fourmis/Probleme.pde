public class Probleme {
  int nbVilles;
  float tauxEvaporation;
  float[][] tabPheromones;
  int[][] tabDistances;
  
  public Probleme(int nbVilles, float tauxEvaporation) {
  this.nbVilles=nbVilles;
  this.tabDistances = new int[this.nbVilles][this.nbVilles];
    this.tabPheromones = new float[this.nbVilles][this.nbVilles];
    this.tabDistances = generateTabDistances();
    this.tauxEvaporation = tauxEvaporation;
    for (int i = 0; i<this.nbVilles;i++) {
      for (int j = 0; j<this.nbVilles;j++) {
        this.tabPheromones[i][j] = 1; //Mise des phéromones à 1
      }
    }
  }
  public int[][] generateTabDistances() {
  int[][] tab = new int[this.nbVilles][this.nbVilles];
    for (int i = 0; i<this.nbVilles; i++){
      tab[i][i] = 0;
      for (int j = i+1; j<this.nbVilles; j++)
        tab[i][j] = tab[j][i] = (int) 1 + (int)(Math.random() * ((50 - 1) + 1));
    }
    return tab;
  }
  
  public void evaporation() {
    for (int i = 0;i<this.nbVilles;i++) {
      for (int j = 0; j< i;j++) {
        tabPheromones[i][j] = tabPheromones[i][j]*(100-tauxEvaporation)/100;
        if (tabPheromones[i][j] < 1.0f) 
          tabPheromones[i][j] = 1.0f;
        tabPheromones[j][i] = tabPheromones[i][j];
      }
    }
  }
}