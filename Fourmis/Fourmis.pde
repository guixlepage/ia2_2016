import java.util.ArrayList;

public class Fourmis {
  ArrayList<Integer> villesVisitees = new ArrayList<Integer>();
  ArrayList<Integer> villesAVisiter = new ArrayList<Integer>();
  ArrayList<Integer> villes = new ArrayList<Integer>();
  int longueurVisitee;
  
  int tailleArcCourant;
  int positionArcCourant; 
  int origine;
  int destination;
  Probleme probleme;
  boolean termine;
  
  int etat; /*vaut 0 si la fourmi vient d'être créée
              vaut 1 si la fourmi cherche son chemin
              vaut 2 si la fourmi est en train de revenir au point de départ*/
  
  public Fourmis(Probleme probleme) {
    etat = 0;
    longueurVisitee = 0;
    tailleArcCourant = 0;
    positionArcCourant = -1;
    origine = 0;
    destination = 0;
    this.probleme = probleme;
    for (int i = 0; i < probleme.nbVilles; i++) {
      villes.add(i);
    }
  }
  
  public boolean mouvement() {
    if(etat == 0) 
      this.rechercheNouveauNoeud();
    else if(etat == 1) {
      longueurVisitee++;
      positionArcCourant++;
      if (positionArcCourant == tailleArcCourant)
        this.rechercheNouveauNoeud();
      
    }
    else {
      this.positionArcCourant++;
      if (this.positionArcCourant >= tailleArcCourant)
        if (this.rechercheNouveauNoeud()) 
          return true;
    }
    return false;
  }
  
  public boolean rechercheNouveauNoeud() {
    
    if (etat == 0) {  //si juste créée
      longueurVisitee = 0;
      villesAVisiter.clear();
      villesVisitees.clear();
      for (int i = 0; i < probleme.nbVilles; i++) { //remplissage des villes à visiter
        villesAVisiter.add(i);
      }
      etat = 1; //passage à l'état de recherche de chemin
      termine = false;
      origine = villes.get(0);
      
      villesAVisiter.remove(origine);
      villesVisitees.add(origine);
      positionArcCourant = 0;
      destination = choisirVille(villes.get(0));
      tailleArcCourant = probleme.tabDistances[origine][destination];
    }
    
    else if (etat == 1) {      //en cours de trajet
      
      //longueurVisitee += probleme.tabDistances[origine][destination];
      villesVisitees.add(villes.get(destination));
      villesAVisiter.remove(villes.get(destination));
      //villesAVisiter.remove(villesAVisiter.indexOf(destination));
      if(villesAVisiter.size() == 0) {
        etat = 2; //passage à l'état de retour
        //longueurVisitee += probleme.tabDistances[destination][villesAVisiter.get(0)];
        origine = villesVisitees.size()-1; //origine : dernière ville visitée
        destination = villesVisitees.size()-2;  //destination : avant-dernière ville visitée
        tailleArcCourant = probleme.tabDistances[villesVisitees.get(origine)][villesVisitees.get(destination)];
        positionArcCourant = tailleArcCourant;
        return false;
      }
      origine = destination;
      destination = choisirVille(origine);
      tailleArcCourant = probleme.tabDistances[origine][destination];
      positionArcCourant = 0;
    }
    
    else {  //if (etat == 2) {
      
      //ajouterPheromones(villesVisitees.get(origine), villesVisitees.get(destination),longueurVisitee);
      if (destination == 0) {
          termine = true;
          return true;
        }
      
        
      origine = destination;
      destination--;
      tailleArcCourant = probleme.tabDistances[origine][destination];
      positionArcCourant = tailleArcCourant;
      
    }
    return false;
  }
  
  public int choisirVille(int villeOrigine) {
  boolean villeTrouvee = false;
    float pheromonesChemin = 0;
    for (int i = 0; i<villesAVisiter.size();i++) {
      if (villesAVisiter.get(i) != villeOrigine) {
        pheromonesChemin += probleme.tabPheromones[villeOrigine][villesAVisiter.get(i)];
      }
    }
    float rand = (int)(Math.random() * ((pheromonesChemin*probleme.nbVilles) + 1))/probleme.nbVilles;
    float pheromones = 0;
    int i = 0;
    while(i < villesAVisiter.size()) {
      if (villesAVisiter.get(i) == villeOrigine) {
        i++;
        continue;
      }
      pheromones += probleme.tabPheromones[origine][villesAVisiter.get(i)];
      if (pheromones > rand) {
      villeTrouvee = true;
        break;
      }
      i++;
    }
    if (!villeTrouvee) {
    int index = (int)(Math.random() * (villesAVisiter.size()));
    return villesAVisiter.get(index);
    }
    return villesAVisiter.get(i);    
  }
  
  public void ajouterPheromones(int i, int j, int longueurChemin) {
    int longueurMin = probleme.nbVilles;
    float pheromones = 100*longueurMin / (longueurChemin + 1 - longueurMin);
    probleme.tabPheromones[i][j] += pheromones;
    
    if(probleme.tabPheromones[i][j] < 1) 
      probleme.tabPheromones[i][j] = 1;
    if(probleme.tabPheromones[i][j] > 100) 
      probleme.tabPheromones[i][j] = 100;
      
    probleme.tabPheromones[j][i] = probleme.tabPheromones[i][j];
  }
}