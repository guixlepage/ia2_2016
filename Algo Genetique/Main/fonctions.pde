import java.io.*; 

ArrayList<Noeud> creerCircuitHamil(ArrayList<Noeud> graphe){
    
    ArrayList<Noeud> circuit = new ArrayList<Noeud>();
   int pointDepart = (int) random(graphe.size());
   circuit.add(graphe.get(pointDepart));
    for(int i=0; i<graphe.size()-1; i++)
    {
      int tmp=0;
      do{     
          tmp =(int) random(graphe.size());
      }while(circuit.contains(graphe.get(tmp)));
      circuit.add(graphe.get(tmp));
    }
    
    return circuit;
}

int trouveMeilleurVoyageur(ArrayList<Voyageur> population){
   int indiceMeilleur = 0;
   for(int i=0; i<population.size(); i++){
     if(population.get(i).coutChemin < population.get(indiceMeilleur).coutChemin)
     {
       indiceMeilleur = i;
     }
   }
   return indiceMeilleur;
}

ArrayList<Voyageur> reproduction(ArrayList<Voyageur> population, Voyageur p1, Voyageur p2){
   ArrayList<Noeud> chemin1 = new ArrayList<Noeud>();
   ArrayList<Noeud> chemin2 = new ArrayList<Noeud>();
   
   for(int i=0 ; i < nbrNoeuds/2; i++)
   {
     chemin1.add(p1.parcours.get(i));
     chemin2.add(p2.parcours.get(i));
   }
   for(int i=0 ; i < nbrNoeuds/2; i++)
   {
     chemin1.add(p1.parcours.get(nbrNoeuds-i-1));
     chemin2.add(p2.parcours.get(nbrNoeuds-i-1));
   }
   
   ArrayList<Voyageur> enfants = new ArrayList<Voyageur>();
   Voyageur tmp = new Voyageur(population.size()+1, chemin1);
   enfants.add(tmp);
   Voyageur tmp2 = new Voyageur(population.size()+1, chemin2);
   enfants.add(tmp2);
   return enfants;
}

ArrayList<Voyageur> reproduction2(ArrayList<Voyageur> population, Voyageur p1, Voyageur p2){
   ArrayList<Noeud> chemin1 = new ArrayList<Noeud>();
   ArrayList<Noeud> chemin2 = new ArrayList<Noeud>();
   
   for(int i=0 ; i < nbrNoeuds/2; i++)
   {
     chemin1.add(p1.parcours.get(i));
     chemin2.add(p2.parcours.get(nbrNoeuds-i-1));
   }
   for(int i=0 ; i < nbrNoeuds; i++)
   {
     if(!(chemin1.contains(p2.parcours.get(i)))){
            chemin1.add(p2.parcours.get(i));
     }
     if(!(chemin2.contains(p1.parcours.get(i)))){
            chemin2.add(p1.parcours.get(i));
     }
   }
   
   ArrayList<Voyageur> enfants = new ArrayList<Voyageur>();
   Voyageur tmp = new Voyageur(population.size()+1, chemin1);
   enfants.add(tmp);
   Voyageur tmp2 = new Voyageur(population.size()+1, chemin2);
   enfants.add(tmp2);
   return enfants;
}

void verifPopulation(ArrayList<Voyageur> population)
{
  for(int i=0; i<population.size(); i++)
  {
    if(!population.get(i).verifHamil())
    {
      population.remove(i);
    }
  }
}

void vieillissement(ArrayList<Voyageur> population)
{
  for(int i=0 ; i<population.size(); i++)
  {
     population.get(i).dureeVie --;
     if(population.get(i).estMort())
     {
       population.remove(i);
     }
  }
}

void mutation(Voyageur newChild2, ArrayList<Noeud> graphe)
{
  int valeurChange = (int) random(graphe.size());
  int changement = (int) random(graphe.size());
  Noeud tmp = graphe.get(changement);
  newChild2.parcours.set(valeurChange,tmp);
}

void AfficheParcours(Voyageur bestVoyageur)
{
  print("Debut Tracage... \n");
  float compteur =0;
  for(int i=1; i<bestVoyageur.parcours.size();i++){
    float x = bestVoyageur.parcours.get(i-1).x;
    float y = bestVoyageur.parcours.get(i-1).y;
    float x1 = bestVoyageur.parcours.get(i).x;
    float y1 = bestVoyageur.parcours.get(i).y;
    line(x,y,x1,y1);
    //print("Je pars de : " + x +" " + y + " et je vais sur (x,y) "+ x1 + " " + y1 +"\n");

  }
  //print("Le chemin a couté : "+ bestVoyageur.coutChemin + "\n" );
}


ArrayList<Integer> tirageParent(ArrayList<Voyageur> population, ArrayList<Integer> tab){
  ArrayList<Integer> retour = new ArrayList<Integer>();
  int v1 = population.get(tab.get(0)).coutChemin;
  int v2 = population.get(tab.get(1)).coutChemin;
  int v3 = population.get(tab.get(2)).coutChemin;
  int v4 = population.get(tab.get(3)).coutChemin;
  
  int meilleur1 = v1;
  int meilleur2 = v2;
  int imeilleur1 = 0;
  int imeilleur2 = 0;
  int indice =0;
  
  int[] tablo = {v1,v2,v3,v3};
  for(int i = 0 ; i<4 ; i++){
    if(meilleur1 > tablo[i]){
     meilleur1 = tablo[i];
     indice = i;
     imeilleur1 = tab.get(i);
    }
  }
  
  for(int i = 0 ; i<4 ; i++){
    if(i != indice){
       if(meilleur1 > tablo[i]){
         meilleur2 = tablo[i];
         indice = i;
         imeilleur2 = tab.get(i);
      }
    }
  }
  
  retour.add(imeilleur1);
  retour.add(imeilleur2);
  return retour;
  
}