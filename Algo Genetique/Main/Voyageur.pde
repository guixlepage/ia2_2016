import java.util.ArrayList;
import java.util.Random;

public class Voyageur {
    int nom;
    ArrayList<Noeud> parcours = new ArrayList<Noeud>();
    int coutChemin = 0;
    int dureeVie = temps / 5;

    public Voyageur(){
      
    }
  
    public Voyageur(int nom, ArrayList<Noeud> chemin){
      this.nom = nom;
      this.parcours = chemin;
      
      //calcul du cout
      int couttmp =0;
      for(int i=1; i<this.parcours.size();i++)
      {
        float x = this.parcours.get(i-1).x;
        float y = this.parcours.get(i-1).y;
        float x1 = this.parcours.get(i).x;
        float y1 = this.parcours.get(i).y;
        couttmp += dist(x,y,x1,y1);
        
      }
      this.coutChemin = couttmp;
    }
    
    public Boolean estMort(){
      if(this.dureeVie == 0){
        return true;
      }
      else
      {
        return false;
      }
    }
    
    void setDureeVie(int i){
     this.dureeVie =i; 
    }
    
    Boolean verifHamil(){
      
      ArrayList<Noeud> cheminVerif = new ArrayList<Noeud>();
       for(int i=0; i<this.parcours.size();i++){
         if(cheminVerif.contains(this.parcours.get(i))){
          return false; 
         }
         else
         {         
           cheminVerif.add(this.parcours.get(i));
         }
       }
       return true;
    }

   
}