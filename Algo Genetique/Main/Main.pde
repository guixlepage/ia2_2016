import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

int nbrNoeuds = 20;  //nombre de ville
int temps = 100;
int nbrCircuitDepart = nbrNoeuds * 2; //nombre de voyageurs de départ
int TauxEnfants = 65; // valeur /100
int TauxMutation = 1; // valeur/1000
int nbrReproduction = nbrCircuitDepart / 3;
/* ------------------------------------------------------------ SETUP ----------------------------------------------------------- */ 
void setup() {
  size(500, 500);  // Size must be the first statement
  stroke(255);     // Set line drawing color to white
  frameRate(30);
  background(0);   // Clear the screen with a black background
  
   int t1 = millis();
  ArrayList<Noeud> graphe = new ArrayList<Noeud>();
  ArrayList<Voyageur> population = new ArrayList<Voyageur>();
  Voyageur bestVoyageur = new Voyageur();
  int nbrChangement =0;
  
  //initialisation des noeuds 
  for(int i=0; i<nbrNoeuds ; i++)
  {
    Noeud tmp = new Noeud(); 
    tmp.name = i;
    tmp.x = random(450);
    tmp.y = random(450);
    graphe.add(tmp);
    tmp.drawNoeud();   
  }
   
   //Trouver les différents circuits
   for(int j=0 ; j<nbrCircuitDepart; j++){
     Voyageur tmp = new Voyageur(j, creerCircuitHamil(graphe));
      population.add(tmp);
   } //<>//
   
   bestVoyageur = population.get(trouveMeilleurVoyageur(population));
   bestVoyageur.setDureeVie(100000); //on le rend invincible
   print("Le meilleur chemin actuel a couté : "+ bestVoyageur.coutChemin + "\n" );
   
   //Selection random des deux parents qui vont se reproduire
   do{
     temps --;
     for(int i=0 ; i < nbrReproduction ; i++)
     {
            //On selectionne deux parents pour reproduction
           int parent1;
           int parent2;
           // A FAIRE TROUVER TIRAGE ROULETTE
           ArrayList<Integer> tab = new ArrayList<Integer>();
            for(int j =0 ; j<population.size(); j++){
              tab.add(j);
            }
            Collections.shuffle(tab);
            parent1= tirageParent(population, tab).get(0);
            parent2= tirageParent(population, tab).get(1);
            
           int randomRepro =(int) random(100);
           if(randomRepro < TauxEnfants)
           {
             //On crée les enfants et ajoute a la population
             Voyageur newChild = new Voyageur();
             newChild = reproduction(population, population.get(parent1),population.get(parent2)).get(0);
             Voyageur newChild2 = new Voyageur();
             newChild2 = reproduction2(population, population.get(parent1),population.get(parent2)).get(1);
             population.add(newChild);
             population.add(newChild2);
             //On fait muter a 0.1% de chance
             int randomMutation =(int) random(1000);
             if(randomMutation < TauxMutation)
             {
               int choixEnfantMutation = (int) random(2);
               if(choixEnfantMutation < 1)
               {
                  mutation(newChild , graphe); 
               }
               else
               {
                 mutation(newChild2 , graphe); 
               }
             } // fin if 
          } //fin if 
     } //fin pour 
    
      //on vérifie la population et enlève les non Hamiltoniens!
      verifPopulation(population);
      //on vieillit la population
      vieillissement(population);
      //On récupère le meilleur chemin actuel
      bestVoyageur.setDureeVie(10);
      Voyageur tmp = new Voyageur();
      tmp = population.get(trouveMeilleurVoyageur(population));
      tmp.setDureeVie(1000000);
      if(bestVoyageur.coutChemin != tmp.coutChemin){
        nbrChangement++;
        print("Le meilleur chemin actuel a couté : "+ tmp.coutChemin + " et il y a eu "+nbrChangement + " changements!"+ "\n" );
      }
      bestVoyageur = tmp;
       
   }while(temps > 0);
    
     int t2 = millis();
     print("Temps execution : " + (t2-t1) +"\n");
    AfficheParcours(bestVoyageur);
    print("Fin du programme...");
   

   
}



/* ------------------------------------------------------------ DRAW ----------------------------------------------------------- */ 
 void draw(){

 
 
}