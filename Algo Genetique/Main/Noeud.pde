import java.util.HashMap;
import java.util.Map;

public class Noeud {
    int name;
    float x,y;

    public Noeud(int name) {
        this.name = name;
    }

    public Noeud(){}

    public void setName(int name) {
        this.name = name;
    }


    public int getName() {
        return name;
    }
    
    public void drawNoeud(){
      ellipse(x , y , 10 , 10);
    }
  
}